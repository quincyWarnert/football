package com.example.simfootball;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatchTest {

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void main(String[] args) {


        List<Match> matchesPlayed = new ArrayList<Match>();
        matchesPlayed.add(new Match(new Team(1, "teamA", 1),new Team(2, "teamB", 2)));
        matchesPlayed.add(new Match(new Team(1, "teamA", 1),new Team(3, "teamC", 3)));
        matchesPlayed.add(new Match(new Team(1, "teamA", 1),new Team(4, "teamD", 4)));
        matchesPlayed.add(new Match(new Team(2, "teamB", 2),new Team(3, "teamC", 3)));
        matchesPlayed.add(new Match(new Team(2, "teamB", 2),new Team(4, "teamD", 4)));
        matchesPlayed.add(new Match(new Team(3, "teamC", 3),new Team(4, "teamD", 4)));

        matchesPlayed.forEach(m -> System.out.println("Printing home: " + m.get_homeTeam().teamName + "   Printing away: " + m.get_awayTeam().teamName));

//        List<Team> mList = new ArrayList<>();
//        matchesPlayed.forEach(a -> mList.addAll(Arrays.asList(a.get_homeTeam(), a.get_awayTeam())));
//        List<Team> allMatches = new ArrayList<>(mList);
        // return allmatches sorted by id

    }



    static class Match {

        private int homeTeamId;
        private int secondTeamId;
        private Team _homeTeam;
        private Team _awayTeam;

        public Match(Team _homeTeam, Team _awayTeam) {
            int MatchId = (int) Math.random();
            this._homeTeam = _homeTeam;
            this._awayTeam = _awayTeam;
        }

        public int getHomeTeamId() {
            return homeTeamId;
        }

        public void setHomeTeamId(int homeTeamId) {
            this.homeTeamId = homeTeamId;
        }

        public int getSecondTeamId() {
            return secondTeamId;
        }

        public void setSecondTeamId(int secondTeamId) {
            this.secondTeamId = secondTeamId;
        }

        public Team get_homeTeam() {
            return _homeTeam;
        }

        public void set_homeTeam(Team _homeTeam) {
            this._homeTeam = _homeTeam;
        }

        public Team get_awayTeam() {
            return _awayTeam;
        }

        public void set_awayTeam(Team _awayTeam) {
            this._awayTeam = _awayTeam;
        }
    }

        static class Team {
            private int teamId;
            private String teamName;
            private int rank;
            private int totalPoints;

            public int totalGoalsFor;
            public int totalGoalsAgainst;

            public Team(int teamId, String teamName, int rank) {
                this.teamId = teamId;
                this.teamName = teamName;
                this.rank= rank;
            }

            public int getTeamId() {
                return teamId;
            }

            public void setTeamId(int teamId) {
                this.teamId = teamId;
            }

            public String getTeamName() {
                return teamName;
            }

            public void setTeamName(String teamName) {
                this.teamName = teamName;
            }

            public int getRank() {
                return rank;
            }

            public void setRank(int rank) {
                this.rank = rank;
            }

            public int getTotalPoints() {
                return totalPoints;
            }

            public void setTotalPoints(int totalPoints) {
                this.totalPoints = totalPoints;
            }

            public int getTotalGoalsFor() {
                return totalGoalsFor;
            }

            public void setTotalGoalsFor(int totalGoalsFor) {
                this.totalGoalsFor = totalGoalsFor;
            }

            public int getTotalGoalsAgainst() {
                return totalGoalsAgainst;
            }

            public void setTotalGoalsAgainst(int totalGoalsAgainst) {
                this.totalGoalsAgainst = totalGoalsAgainst;
            }
        }





}
