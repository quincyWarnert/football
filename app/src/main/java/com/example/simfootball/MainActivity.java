package com.example.simfootball;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.simfootball.methodes.GenerateMatch;
;
import com.example.simfootball.methodes.GenerateTeam;
import com.example.simfootball.methodes.SimMatch;
import com.example.simfootball.models.Match;

import com.example.simfootball.models.Team;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ConsoleLog";
    List<Match> listOfMatches = new ArrayList<>();
    List<Team> list = new ArrayList<>();


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = new GenerateTeam().GenerateTeams();

        List<Match> matchesToBePlayed = new GenerateMatch().Generatematch(list);
        Button btn = findViewById(R.id.button);
        Button btn2 = findViewById(R.id.button2);
        Button reset = findViewById(R.id.resetButton);
        btn.setOnClickListener(view -> startmatch(matchesToBePlayed));
        btn2.setOnClickListener(view -> resulttable(listOfMatches));
        reset.setOnClickListener(view -> resetSim());


    }

    //Starts a simulating the matches with a list of fixtures from the generateMatch class
    public void startmatch(List<Match> matchesToBePlayed) {

        for (Match match : matchesToBePlayed
        ) {
            Match startSim = new SimMatch().SimulateMatch(match);
            listOfMatches.add(startSim);

        }


        fixturesList(listOfMatches);

    }

    // Shows the fixture list
    public void fixturesList(List<Match> fixtures) {
        leaderboard(list);
        TableLayout tableLayout = findViewById(R.id.tablelayout);

        for (Match fixture : fixtures
        ) {
            String home = fixture.get_homeTeam().getTeamName();
            String away = fixture.get_awayTeam().getTeamName();

            TableRow row = new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            TextView game = new TextView(this);
            game.setText(new StringBuilder(home + " - " + away));
            row.addView(game);
            tableLayout.addView(row, 1);
        }

    }

    // Dynamically creates the fixture table with the results
    public void resulttable(List<Match> teamList) {

        TableLayout tableLayout = findViewById(R.id.tablelayout);

        TableRow row = new TableRow(this);

        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        TextView header = new TextView(this);
        header.setText(new StringBuilder("Team|GOALS"));
        row.addView(header);
        for (Match team : teamList
        ) {
            TableRow row2 = new TableRow(this);
            String homeTeam = team.get_homeTeam().getTeamName();
            int homeTeamScore = team.getTeam1score();

            String awayTeam = team.get_awayTeam().getTeamName();
            int awayTeamScore = team.getTeam2score();

            TextView homeTeamString = new TextView(this);
            homeTeamString.setText(new StringBuilder(homeTeam + ": " + homeTeamScore + " - " + awayTeam + ":" + awayTeamScore));
            row2.addView(homeTeamString);
            Log.d(TAG, "resulttable: " + homeTeamString.toString());


            if (row.getParent() != null) {
                ((ViewGroup) row.getParent()).removeView(row);
            }
            tableLayout.addView(row, 7);

            if (row2.getParent() != null) {
                ((ViewGroup) row2.getParent()).removeView(row2);
            }
            tableLayout.addView(row2, 8);
        }


    }

    //Creates and sorts the leaderboard by total points
    public void leaderboard(List<Team> mlist) {
        Collections.sort(mlist);
        TableLayout tableLayout = findViewById(R.id.tablelayout);
        for (Team team : mlist
        ) {
            TableRow row2 = new TableRow(this);
            String name = team.getTeamName();
            int points = team.getTotalPoints();
            int goals = team.getTotalGoalsFor();
            int goalAg = team.getTotalGoalsAgainst();
            int goalDif = team.goalDifference();
            TextView positionString = new TextView(this);
            positionString.setText(new StringBuilder(name + " : Pts: " + points + " |  G/F: " + goals + " |  G/A: " + goalAg + " | G/D: " + goalDif));
            row2.addView(positionString);
            if (row2.getParent() != null) {
                ((ViewGroup) row2.getParent()).removeView(row2);
            }
            tableLayout.addView(row2, 2);

        }
    }


    // reset the simulator and screen*
    public void resetSim() {
        listOfMatches.clear();
        finish();
        startActivity(getIntent());

    }


}