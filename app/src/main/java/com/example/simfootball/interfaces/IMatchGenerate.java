package com.example.simfootball.interfaces;

import com.example.simfootball.models.Match;
import com.example.simfootball.models.Team;

import java.util.List;

public interface IMatchGenerate {
    List<Match> Generatematch(List<Team>Teams);
}
