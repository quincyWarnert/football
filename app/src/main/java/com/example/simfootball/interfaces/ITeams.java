package com.example.simfootball.interfaces;


import com.example.simfootball.models.Team;

import java.util.List;

public interface ITeams {
    List<Team> GenerateTeams();

}
