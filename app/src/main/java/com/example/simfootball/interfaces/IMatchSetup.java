package com.example.simfootball.interfaces;

import com.example.simfootball.models.Match;

public interface IMatchSetup {
    Match SimulateMatch(Match match);
}
