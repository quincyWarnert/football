package com.example.simfootball.models;

import java.util.Collection;
import java.util.UUID;

import static java.util.UUID.randomUUID;

public class Team implements Comparable<Team> {
    private UUID teamId;
    private String teamName;
    private int rank;
    private int totalPoints;

    public int totalGoalsFor;
    public int totalGoalsAgainst;

    public Team(UUID teamId, String teamName, int rank) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.rank= rank;
    }



    public UUID getTeamId() {
        return teamId;
    }

    private void setTeamId(UUID teamId) {
        this.teamId = teamId;
    }

    public int getRank() {
        return rank;
    }

    private void setRank(int rank) {
        this.rank = rank;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int goalDifference() {

        return totalGoalsFor - totalGoalsAgainst;
    }

    public int getTotalGoalsFor() {
        return totalGoalsFor;
    }

    public void setTotalGoalsFor(int totalGoalsFor) {
        this.totalGoalsFor = totalGoalsFor;
    }

    public int getTotalGoalsAgainst() {
        return totalGoalsAgainst;
    }

    public void setTotalGoalsAgainst(int totalGoalsAgainst) {
        this.totalGoalsAgainst = totalGoalsAgainst;
    }

    public void AddTotalPoints(int points) {
        totalPoints += points;
    }



    @Override
    public int compareTo(Team team) {
        return Integer.compare(this.getTotalPoints(), team.getTotalPoints());
    }

}
