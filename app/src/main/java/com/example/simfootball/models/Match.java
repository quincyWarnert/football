package com.example.simfootball.models;

import java.util.UUID;

import static java.util.UUID.randomUUID;

public class Match {
    private int homeTeamId;
    private int secondTeamId;
    private Team _homeTeam;
    private Team _awayTeam;

    private int team1score;
    private int team2score;


    public Match(Team _homeTeam, Team _awayTeam) {
        UUID MatchId = randomUUID();
        this._homeTeam = _homeTeam;
        this._awayTeam = _awayTeam;
    }

    public Team get_homeTeam() {
        return _homeTeam;
    }

    public void set_homeTeam(Team _homeTeam) {
        this._homeTeam = _homeTeam;
    }

    public Team get_awayTeam() {
        return _awayTeam;
    }

    public void set_awayTeam(Team _awayTeam) {
        this._awayTeam = _awayTeam;
    }

    public int getHomeTeamId() {
        return homeTeamId;
    }

    private void setHomeTeamId(int homeTeamId) {
        this.homeTeamId = homeTeamId;
    }

    public int getSecondTeamId() {
        return secondTeamId;
    }

    private void setSecondTeamId(int secondTeamId) {
        this.secondTeamId = secondTeamId;
    }

    public int getTeam1score() {
        return team1score;
    }

    public void setTeam1score(int team1score) {
        this.team1score = team1score;
    }

    public int getTeam2score() {
        return team2score;
    }

    public void setTeam2score(int team2score) {
        this.team2score = team2score;
    }
}
