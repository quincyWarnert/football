package com.example.simfootball.methodes;

import com.example.simfootball.interfaces.IMatchGenerate;
import com.example.simfootball.models.Match;
import com.example.simfootball.models.Team;

import java.util.ArrayList;
import java.util.List;

public class GenerateMatch implements IMatchGenerate {
    @Override
    public List<Match> Generatematch(List<Team> Teams) {
        // Create a match using the List from generate team
        List<Match> teamMatches = new ArrayList<>();
        for (int i = 0; i < Teams.size(); i++) {
            for (int p = i; p < Teams.size() - 1; p++) {
                    Match _match = new Match(Teams.get(i), Teams.get(p + 1));
                    teamMatches.add(_match);
            }
        }
        return teamMatches;
    }
}
