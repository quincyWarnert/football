package com.example.simfootball.methodes;


import android.util.Log;

import com.example.simfootball.interfaces.IMatchSetup;
import com.example.simfootball.models.Match;
import com.example.simfootball.models.Team;

import java.util.Random;

public class SimMatch implements IMatchSetup {
    private static final String TAG ="simlog" ;
    private Match _match;
    private Team teamA;
    private Team teamB;

    public Match get_match() {
        return _match;
    }

    public void set_match(Match _match) {
        this._match = _match;
    }

    @Override
    public Match SimulateMatch(Match match) {
        this._match = match;
        teamA = _match.get_homeTeam();
        teamB = _match.get_awayTeam();
        double teamARank = teamA.getRank();
        double teamBRank = teamB.getRank();

        int mightScore = (int) (Math.random() * 10);
        //If the teamA is ranked higher than teamB
        //They have a higher chances of scoring

            if (teamARank < teamBRank) {
                //chance of scoring
                Log.d(TAG, "SimulateMatch: " + teamA.getTeamName() + " " + teamB.getTeamName());
                if (mightScore >= 3) {
                    _match.setTeam1score(1);

                } else {
                    _match.setTeam2score(1);

                }
            }
            //If the teams are equally matched
            if (teamARank == teamBRank) {
                //chance of scoring
                if (mightScore >= 5) {
                    _match.setTeam1score(1);

                } else {
                    _match.setTeam2score(1);

                }
            }

        Match getMatch = get_match();

        return matchResults(getMatch);

    }//End of simulate match

    
    public Match matchResults(Match match) {

        match.get_homeTeam().totalGoalsFor += match.getTeam1score();
        match.get_homeTeam().totalGoalsAgainst += match.getTeam2score();

        match.get_awayTeam().totalGoalsFor += match.getTeam2score();
        match.get_awayTeam().totalGoalsAgainst += match.getTeam1score();

        if (match.getTeam1score() > match.getTeam2score()) {
            match.get_homeTeam().AddTotalPoints(3);
        } else if (match.getTeam1score() < match.getTeam2score()) {
            match.get_awayTeam().AddTotalPoints(3);
        } else {
            match.get_homeTeam().AddTotalPoints(1);
            match.get_awayTeam().AddTotalPoints(1);
        }
        Log.d(TAG, "GetMatch: "+match.get_homeTeam().getTeamName()+" + "+match.get_awayTeam().getTeamName());
        return match;
    }

}

