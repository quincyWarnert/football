package com.example.simfootball.methodes;

import com.example.simfootball.interfaces.ITeams;
import com.example.simfootball.models.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.UUID.randomUUID;

public class GenerateTeam implements ITeams {
    @Override
    public List<Team> GenerateTeams() {


        List<Team> teams = new ArrayList<>();
        Team teamA = CreateTeam("Netherlands",16);
        teams.add(teamA);
        Team teamB = CreateTeam("Ukraine",24);
        teams.add(teamB);
        Team teamC = CreateTeam("Austria",23);
        teams.add(teamC);
        Team teamD = CreateTeam("North Macedonia",62);
        teams.add(teamD);

        return teams;
    }

    private Team CreateTeam(String name,int rank){
    return new Team(randomUUID(),name,rank);

    }
}
